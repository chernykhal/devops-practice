## First lesson

Будут проигнорированы:
1. все директории .terraform их содержимое
2. все файлы с расширением .tfstate 
3. все crash.log и crash.*.log(ex. crash.2022-01-01.log)
4. все файлы с расширением .tfvars
5. файлы override.tf
   override.tf.json
   *_override.tf(ex. 2021-01-01_override.tf)
   *_override.tf.json(ex. 2021-01-01_override.json)
6. файлы .terraformrc и terraform.rc
